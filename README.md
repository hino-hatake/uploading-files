### 1. Prerequisites
- `docker` 20.10+, `docker-compose` 1.29+
- `BuildKit` enabled, update your `/etc/docker/daemon.json`:
  ```json
  {
    "features": {
      "buildkit": true
    }
  }
  ```

### 2. Just build and deploy the normal way
Build jar with `mvn` cli:
```bash
mvn clean package
mvn clean package -DskipTests
```

Or use the maven image:
```bash
docker run --rm -it -v "$PWD":/working -w /working -v "$HOME/.m2":/root/.m2 maven:3.6-jdk-8-alpine mvn clean package -DskipTests
```

Or even run with non-root user:
```bash
docker run --rm -it -v "$PWD":/working -w /working -v ~/.m2:/var/maven/.m2 -u 1000 -e MAVEN_CONFIG=/var/maven/.m2 maven:3.6-jdk-8-alpine mvn -Duser.home=/var/maven clean package -DskipTests
```

Stand them up:
```bash
docker-compose up -d --build && docker-compose logs -f
```

### 3. (alternative) Multi-stage builds and BuildKit inline cache
Not that effective so far, but let's give the community some time :crocodile:

First, build your cache image:
```bash
docker build -f multi-stage.Dockerfile --target build --cache-from hinorashi/uploading-files:cache --tag hinorashi/uploading-files:cache --build-arg BUILDKIT_INLINE_CACHE=1 .
```

Now build your main image:
```bash
docker-compose build
```

Push them to registry so that builds from other machines can leverage it (not so sure :cactus:):
```bash
docker push hinorashi/uploading-files:cache
docker push hinorashi/uploading-files
```

Stand them up:
```bash
docker-compose up -d && docker-compose logs -f
```
