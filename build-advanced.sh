#!/bin/sh
docker run --rm -it \
  -v "$PWD":/working -w /working \
  -v ~/.m2:/var/maven/.m2 \
  -u 1000 -e MAVEN_CONFIG=/var/maven/.m2 \
  maven:3.6-jdk-8-alpine \
  mvn -Duser.home=/var/maven clean package -DskipTests

