# syntax=docker/dockerfile:1
FROM openjdk:8-jre-alpine

LABEL maintainer="Hino <sinhngay3110@gmail.com>"

COPY ./target/uploading-files-*.jar /app.jar

ARG STORAGE_LOCATION=/upload-dir

VOLUME $STORAGE_LOCATION

CMD ["java", "-jar", "/app.jar"]

