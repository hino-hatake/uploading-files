package one.hino.upload.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

  /**
   * Folder location for storing files
   */
  private String location = "upload-dir";

  /**
   * Whether to clear the upload dir on init
   */
  private boolean deleteAllOnInit = true;

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public boolean isDeleteAllOnInit() {
    return deleteAllOnInit;
  }

  public void setDeleteAllOnInit(boolean deleteAllOnInit) {
    this.deleteAllOnInit = deleteAllOnInit;
  }

}
