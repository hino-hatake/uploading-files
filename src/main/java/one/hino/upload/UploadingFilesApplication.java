package one.hino.upload;

import one.hino.upload.storage.StorageProperties;
import one.hino.upload.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class UploadingFilesApplication {

  public static void main(String[] args) {
    SpringApplication.run(UploadingFilesApplication.class, args);
  }

  @Bean
  CommandLineRunner init(StorageService storageService, StorageProperties storageProperties) {
    return (args) -> {
      if (storageProperties.isDeleteAllOnInit()) {
        log.warn("Delete all on init!");
        storageService.deleteAll();
      }
      storageService.init();
    };
  }

}
