package one.hino.upload;

import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import one.hino.upload.storage.StorageException;
import one.hino.upload.storage.StorageFileNotFoundException;
import one.hino.upload.storage.StorageService;

@Controller
public class FileUploadController {

  private final StorageService storageService;

  private final MultipartProperties multipartProperties;

  @Autowired
  public FileUploadController(StorageService storageService, MultipartProperties multipartProperties) {
    this.storageService = storageService;
    this.multipartProperties = multipartProperties;
  }

  @GetMapping("/")
  public String listUploadedFiles(Model model) {

    model.addAttribute("files",
        storageService.loadAll()
            .map(path -> MvcUriComponentsBuilder
//                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString()).scheme("https").build().toUri()
                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString()).build().toUri()
                .toString())
            .collect(Collectors.toList()));

    return "upload-form";
  }

  @GetMapping("/files/{filename:.+}")
  @ResponseBody
  public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

    Resource file = storageService.loadAsResource(filename);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
  }

  @PostMapping("/")
  public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

    storageService.store(file);
    redirectAttributes.addFlashAttribute("filename", file.getOriginalFilename());

    return "redirect:/";
  }

  @ExceptionHandler(StorageFileNotFoundException.class)
  public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException ex) {
    return ResponseEntity.notFound().build();
  }

  @ExceptionHandler(StorageException.class)
  public String handleStorageEx(StorageException ex, RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute("error", "Please choose a file before clicking Upload button!");
    return "redirect:/";
  }

  /**
   * handle {@link SizeLimitExceededException} or
   * {@link MaxUploadSizeExceededException} here are both fine
   */
  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public String handleMaxSizeExceededEx(MaxUploadSizeExceededException ex, RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute("error",
        "Maximum upload size exceeded " + multipartProperties.getMaxFileSize().toKilobytes() + "KB !");
    return "redirect:/";
  }

}
