# syntax=docker/dockerfile:1
FROM maven:3.6-jdk-8-alpine AS build

LABEL maintainer="Hino <sinhngay3110@gmail.com>"

WORKDIR /code

COPY pom.xml /code/pom.xml

#RUN --mount=type=cache,target=/root/.m2 \
#  ["mvn", "dependency:resolve", "dependency:resolve-plugins"]
RUN ["mvn", "dependency:resolve", "dependency:resolve-plugins"]

# Adding source, compile and package into a fat jar
COPY ["src/main", "/code/src/main"]

#RUN --mount=type=cache,target=/root/.m2 \
#  ["mvn", "package", "-DskipTests"]
RUN ["mvn", "package", "-DskipTests"]

FROM openjdk:8-jre-alpine

COPY --from=build /code/target/uploading-files-*.jar /app.jar

ARG STORAGE_LOCATION=/upload-dir

VOLUME $STORAGE_LOCATION

CMD ["java", "-jar", "/app.jar"]

