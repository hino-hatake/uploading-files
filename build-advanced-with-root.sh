#!/bin/sh
docker run --rm -it \
  -v "$PWD":/working -w /working \
  -v "$HOME/.m2":/root/.m2 \
  maven:3.6-jdk-8-alpine \
  mvn clean package -DskipTests

